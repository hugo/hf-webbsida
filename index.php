<?php include "content/head.php"; ?>

<article>
<h2>Häfvbastu!</h2>

Den 14 septemebr är det dags för terminens första häfvbastu!

</article>

<hr/>

<article>
<h2>Bästa Häfvtiderna!</h2>
<table style="width:100%;">
<thead>
	<tr>
		<th>#</th>
		<th>Namn</th>
		<th>Tid</th>
		<th>När</th>
	</tr>
</thead>
<tbody>
<?php
$times = $mysql->query(
	"SELECT name, time, performance_date FROM times
	 ORDER BY time ASC")
	or die($mysql->error);
$i = 1;

while ($row = $times->fetch_assoc()) {  ?>
<tr>
	<td><?= $i++ ?>
	<td><?= $row["name"] ?></td>
	<td><?= sprintf("%.2f", $row["time"] / 1000) ?></td>
	<td><?= substr($row["performance_date"], 0, 10) ?></td>
</tr>
<?php } ?>
</tbody>
<?php if ($authenticated) { ?>
<tfoot>
<form method="POST" action="code/add_time.php">
<tr>
<td></td>
<td><input name="name" type="text" placeholder="Namn"/></td>
<td><input name="time" type="number" min="0" max="10" step="0.01" placeholder="Tid" /></td>
<td><input name="when" type="date" value="<?php echo date('Y-m-d'); ?>" /></td>
</tr>
<td></td><td colspan="3"><input style="width: 100%" type="submit"/></td>
<tr>
</tr>
</form>
</tfoot>
<?php } ?>
</table>

</article>

<?php include "content/tail.php"; ?>
