<?php

include "base.php";

global $mysql;
global $authenticated;

if ($authenticated) {
	$stmt = $mysql->prepare(
		"INSERT INTO times (name, time, performance_date)
		 VALUES (?, ?, ?)") or die($mysql->error);
	$time = $_REQUEST["time"] * 1000;
	$stmt->bind_param("sss", $_REQUEST["name"], $time, $_REQUEST["when"]);
	$stmt->execute() or die($mysql->error);
	$stmt->close();

} else {
	die("Not logged in");
}

header("Location: /");
die();
