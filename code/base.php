<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);
// error_reporting(~E_STRICT);

$mysql = new mysqli("localhost", "drain", "securepassword", "drain")
or die(mysqli_connect_error());

$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

function create_user($username, $real_name, $password) {
	global $mysql;

	$salt =  substr(md5(rand()), 0, 64);

	$hash = hash ("sha256", $salt . $password);

	$stmt = $mysql->prepare(
		"INSERT INTO users (username, full_name, salt, hash)
		 VALUES (?, ?, ?, ?)") or die($mysql->error);
	$stmt->bind_param("ssss", $username, $real_name, $salt, $hash);
	$stmt->execute() or die($mysql->error);
	$stmt->close();
}

function login_user($name, $password) {
	global $mysql;
	$stmt = $mysql->prepare(
		"SELECT sha2(concat(salt, ?), 256), hash
		FROM users WHERE username = ?") or die($mysql->error);
	$stmt->bind_param("ss", $password, $name);
	$stmt->execute() or die($mysql->error);
	$stmt->bind_result($h1, $h2);
	$stmt->fetch() or die($mysql->error);
	$stmt->close();
	if ($h1 === $h2) {
		$stmt = $mysql->prepare(
			"INSERT INTO session_cookies (username, best_before, hash)
			 VALUES (?, ?, ?)") or die($mysql->error);

		// next month
		$best_before = date("Y-m-d H:i:s", time() + 3600 * 24 * 7 * 4);

		$stmt->bind_param("sss", $name, $best_before, $h1);
		$stmt->execute() or die($mysql->error);
		$stmt->close();

		return "name=$name&hash=$h1";
	} else {
		return False;
	}
}

function auth_cookie($cookie) {
	global $mysql;
	parse_str($cookie, $output);
	$stmt = $mysql->prepare(
		"SELECT hash FROM session_cookies
		 WHERE username = ?
		 ") or die($mysql->error);
	$stmt->bind_param ("s", $output["name"]);
	$stmt->bind_result($hash);
	$stmt->execute() or die($mysql->error);
	$row = $stmt->fetch();
	$stmt->close();

	if ($output["hash"] === $hash) {
		$stmt = $mysql->prepare("SELECT full_name FROM users WHERE username = ?") or die($mysql->error);
		$stmt->bind_param("s", $output["name"]);
		$stmt->execute() or die($mysql->error);
		$stmt->bind_result($name);
		$stmt->fetch() or die($mysql->error);
		return array(
			"name" => $name,
			"username" => $output["name"]);
	} else {
		return False;
	}
}

$authenticated = False;
if (array_key_exists("login", $_COOKIE)) {
	$authenticated = auth_cookie($_COOKIE["login"]);
}	
