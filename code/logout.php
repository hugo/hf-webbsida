<?php

include "base.php";

global $authenticated;

if ($authenticated) {
	global $mysql;
	unset($_COOKIE["login"]);
	setcookie("login", null);

	$stmt = $mysql->prepare("DELETE FROM session_cookies WHERE username = ?") or die($mysql->error);
	$stmt->bind_param("s", $authenticated["username"]);
	$stmt->execute() or die($mysql->error);

	header("Location: /");
	die();
}
