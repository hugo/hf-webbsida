<?php include "code/base.php" ?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width; initial-scale=1.0"/>
	<link rel="stylesheet" href="/content/style/style.css"/>
	<title>Häfvfakultet</title>
</head>
<body>

<header>
<div class="left">
<h2>Häfvfakultet!</h2>
<?php
global $authenticated;
if ($authenticated) { ?>
	<p>Välkommen, <?= $authenticated["name"] ?></p>
<?php } else { ?>
	<p>Häfv nollan, nollan häfv!</p>
<?php } ?>
</div>
<img src="/content/img/hf-logo.png" alt="HF Logo"/>
</header>

<nav>
<ul>
<a href="/"><li>Startsidan</li></a>
<a href="/content/medlem.php"><li>Bli medlem</li></a>
<a href="/content/stadgar.php"><li>Stadgar</li></a>
<a href="/content/regler.php"><li>Reglemente</li></a>
<a href="https://www.facebook.com/hafvfakultet"><li class="facebook">Facebook</li></a>
<hr/>
<?php if($authenticated) { ?>
<li><a href="/code/logout.php">Log Out</a></li>
<?php } else { ?>
<li><a href="/content/admin.php">Log in</a></li>
<?php } ?>
<li><a href="/content/admin.php">Admin</a></li>
</ul>

</nav>

<main>

