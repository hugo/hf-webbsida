<?php include "head.php"; ?>

<article>

<?php if (isset($_GET["errmsg"])) { ?>
<div class="error-msg"><?= $_GET["errmsg"] ?></div>
<?php } ?>

<?php if (! $authenticated) { ?>
<form method="POST" action="/code/login.php" class="loginform">
<label>Username:</label><input type="text" name="username"/>
<label>Password:</label><input type="password" name="password">
<input type="hidden" name="returnaddr" value="<?= $url ?>"/>
<input type="submit" value="Log in"/>
</form>
<?php } else { ?>

<h2>Create User</h2>
<form method="POST" action="create_user.php" class="loginform">
<label>Username:</label><input type="text" name="username"/>
<label>Real Name:</label><input type="text" name="real_name"/>
<label>Password:</label><input type="password" name="password">
<label>Repeat Password:</label><input type="password" name="password2">
<input type="hidden" name="returnaddr" value="<?= $url ?>"/>
<input type="submit" value="Create"/>
</form>

<?php } ?>

</article>

<?php include "tail.php"; ?>

