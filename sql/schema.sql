CREATE TABLE times (
	id INT(11) AUTO_INCREMENT,
	name VARCHAR(1023) NOT NULL,
	time INTEGER NOT NULL,
	performance_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id)
);

CREATE TABLE users (
	id INT(11) AUTO_INCREMENT,
	username varchar(1023) NOT NULL,
	full_name varchar(1023),
	salt char(64) NOT NULL,
	hash char(64) NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (username)
);

CREATE TABLE session_cookies (
	id INT(11) AUTO_INCREMENT,
	username varchar(1023) NOT NULL,
	best_before TIMESTAMP NOT NULL,

	hash char(64) NOT NULL,

	PRIMARY KEY (id)
);
